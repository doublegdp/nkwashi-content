---
title: Home
link: https://app.doublegdp.com
google_analytics: ['UA-150647211-2', 'app.doublegdp.com']
---
# Nkwashi News

Here we feature updates and policies relevant to the Nkwashi community. We'll be adding new features regularly. Here is some of the content you can see today:

### 2020-05-26
1. We shared an [interview with Miss Mwape Bwalya](posts/2020-05-26-miss-mwape-interview/) about her experience starting to build her home.
2. We announced some great promotions for [new clients](promos/2020-05-26-new-client-discount/), [phase 1 clients](promos/2020-05-phase1/), [phase 2 clients](promos/2020-05-phase2/), and [sublease clients](promos/2020-05-sublease/). Take advantage of yours today!

### 2020-05-18
1. We interviewed Nkwashi’s main contractor, Eligor Zamia, who’s currently building 6 homes on site. They offer full-service and high quality construction. See here to [learn more and see the conversation](/nkwashi-content/posts/2020-05-18-eligor-interview/).

### 2020-05-05
We added a couple of new features to this Nkwashi application:

1. <a href="https://app.doublegdp.com/contact" target="_top">Client Request Forms</a> are available within the app for issues like change of ownership, reactivations, or plot detail changes.
1. Your payment portal is now available within the new <a href="https://app.doublegdp.com/account" target="_top">Account Management</a> card. Note that you will need to log into the portal with your Thebe credentials.

### 2020-04-27
1. We're happy to share our [construction updates](posts/2020-03-31-construction-update/) for March of 2020. Please take a look and <a href="https://app.doublegdp.com/contact" target="_top">share your feedback</a>.
1. We're offering a promotion between now and 31st May that can earn you a discount on your plot payments. Terms and conditions may apply, so please <a href="https://app.doublegdp.com/contact" target="_top">reach out</a> to ask about your eligibility.
    * Take advantage of our cashless payment modes such as the My-thebe online portal, bank transfers and MTN Mobile Money option on 0961722433 indicating your NRC as a reference for identification. Soar High!
1. [Nkwashi Gate Access Policies](posts/gate-access-policy/) have been updated to reflect covid-19 preparedness. In order to reduce risk of spreading the virus, guards are taking the temperature of visitors entering the main site gate. Those with a temperature over 37º C will be asked not to enter.

### 2020-03-29

1. We added new features to the app in our [March 29 Feature Release](posts/2020-03-29-feature-release/)
1. We launched a [Feature Survey](posts/survey/) to collect client input on what we should build next

#### And more
Of course you are always welcome to share your feedback directly through our <a href="https://app.doublegdp.com/contact" target="_top">contact page</a> . Thank you for visiting.
