---
title: Profile of Jacob Seer
google_analytics: ['UA-150647211-2', 'air-jacob-seer']
---

# Jacob Seer

<img src="https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1582831234551-R1GU9KB6A491WII1KNTZ/ke17ZwdGBToddI8pDm48kHUZxYMFIl6KfYlDl4UZ52YUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8PaoYXhp6HxIwZIk7-Mi3Tsic-L2IOPH3Dwrhl-Ne3Z2KgW4FAd3DCUFbQKpgrrVMeT9STXNnolRG_gnaoSvjV2btxPRH917Fz9Qv_NunPol/WhatsApp+Image+2020-02-18+at+4.18.03+PM.jpeg?format=750w" width="250"/>

&nbsp; 

Jacob S. is a contemporary realist artist. He uses charcoal, watercolour, graphite and acrylics to create realistic images of the human figure with occasional abstraction. Jacob's high contrast style depicts the human as a bridge to link the viewer to his work. Jacob’s pieces focus on mental health issues. 

> "I started about three years ago and enjoy hyperrealism, figurative abstractism, anatomy and human figures. What I am most excited about for this program is connecting with other artists and learning."

![Jacob Seer Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1583157759412-17QVIBCEO8H6E613GSE5/ke17ZwdGBToddI8pDm48kOctDx9vyAOfWmHB8XyJswwUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8PaoYXhp6HxIwZIk7-Mi3Tsic-L2IOPH3Dwrhl-Ne3Z23Un09hVPrQgx8ic-Sw8pry67MH1yYRwvTc_QGi878xzvOzbI-NzUQK-lrlmb-Qyr/WhatsApp%2BImage%2B2020-02-17%2Bat%2B9.32.07%2BPM.jpg?format=1000w)


![Jacob Seer Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1584131141893-0NH8A3CK2IXYCMVYEFLA/ke17ZwdGBToddI8pDm48kClCYjIHlAdJFMd9nYld_24UqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcoeLUq1KE8Y9v-dn6oDdYHR-R9p9ZgBAk4eWEys2m57EPbDH7ASf5A5LcwwVgfzyK/WhatsApp+Image+2020-02-17+at+9.32.07+PM+%281%29.jpeg?format=1000w)

![Jacob Seer Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1584131144569-4Y6U5I2WRJ6A8B1Q101U/ke17ZwdGBToddI8pDm48kDNYyMsl_GM94stMfq-YDIkUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcN9hdAlmcG6pn_8PsOvFMNuOZf7YdZ3Fm1tGeIJrOuw-R464gMyiZki5XaXl6Zg-W/WhatsApp+Image+2020-02-17+at+9.32.07+PM+%282%29.jpeg?format=1000w)

