---
title: Profile of Alexander Milambo
google_analytics: ['UA-150647211-2', 'air-alexander-milambo']
---

# Alexander Milambo

<img src="https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1589259152740-PH7URC6O9N1QQSCJBSX2/ke17ZwdGBToddI8pDm48kBJRxpoN9CTx8kf9hgpxFPtZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZamWLI2zvYWH8K3-s_4yszcp2ryTI0HqTOaaUohrI8PIVVLHxevOZPODNSMWTh8fxK32Dtd_b6FlbkYacaAwtkgKMshLAGzx4R3EDFOm1kBS/alexander.jpg" width="250"/>

&nbsp; 

Much like his art, Alex is in touch with his surroundings. He is another level of peace and his work makes one picture a rain forest full of trees and birds, beautiful scenery and peace. He sees the world in a different way and his art is a reflection of this.

> “I was born in Zambia and I’ve been painting professionally for about 6-7 months ago. I enjoy lots of scenery, bodies of water, landscapes, trees and animals. I am excited for this program and look forward to learning a new technique and incorporating natural objects into my work.”

![Alexander Milambo Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1589259434955-M83NVFXB1X4CJI7J2TYN/ke17ZwdGBToddI8pDm48kHRlfU3yGiiYwt_fAxdznSNZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwI-EkWLd3PnwarYOUeSjWDIDM8wD3T_3h6Lkp_-0fznuqTDKmkEWY-V-xFCV5m0b0/alex4.jpeg)

![Alexander Milambo Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1589259431814-DC7Z91CT5GZN56CVLGXB/ke17ZwdGBToddI8pDm48kDufmA-TNONzlolkngnmKFFZw-zPPgdn4jUwVcJE1ZvWEtT5uBSRWt4vQZAgTJucoTqqXjS3CfNDSuuf31e0tVEr5dP_w9tDcgzaRNYhKItNBElYqys7nUv7hRnK5qxNSmojgHrBsX9HavHvgQKgwJs/alex+chena.jpeg?format=1000w)

![Alexander Milambo Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1589259430855-27YKJWV286X24PAMQQ8W/ke17ZwdGBToddI8pDm48kC8dr1lWx-Vu6bJhDqDrL75Zw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwF0vsjHxatfVK5mrxXaTwXgc8zcgPeU6-B6ZszJAD1_XGRSyuYHWSTHCXYVq6U0oY/alex3.jpeg?format=750w)

![Alexander Milambo Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1589259433428-YS5O429E7D1JV5GHEUQD/ke17ZwdGBToddI8pDm48kC4qaUK8a02wJuBP8eSKPoxZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpylzwoqX3GOz3_RXBvmmqqA1AKbAGeIXpdn9xroPbaaivzYhHvN1XN_QcJbBd-Obag/alex2.jpeg?format=750w)
