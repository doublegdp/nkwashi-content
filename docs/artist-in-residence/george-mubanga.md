---
title: Profile of George Mubanga
google_analytics: ['UA-150647211-2', 'air-george-mubanga']
---

# George Mubanga

<img src="https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1582575774566-HQ8KWRZBEL5JUGKAMVTI/ke17ZwdGBToddI8pDm48kLRnyuzmvx3ky12zVjQRvDpZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpwaViZmK0-0CqfvHDXGuAFWGQPT8u79b8VpbnHZ7wzIhzzsWPCWj81HZHhrJUH5q9g/WhatsApp+Image+2020-02-19+at+11.40.47+AM.jpeg" width="250"/>

&nbsp; 

With a cultural understanding of Zambian traditions and ceremonies, George shares a multitude of whimsical, ethnic, and heartwarming experiences as he takes us on a journey, and shares the expressions, thoughts, and feelings of Zambia from his perspective.

> "My goal for this program is to teach and promote understanding of my style of art and bring an audience to my work."

<img src="https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1583157594577-HFC8MXYAAMEAV2APF6O0/ke17ZwdGBToddI8pDm48kMuO5yOWDFxa-BhTtMwu9NhZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpycY7q6V4P7ramvgeLdeAz7LyjnA8rgVA5Iz_5dL7jFXIe6gaJNCGl9Oqi9rXhDFPY/WhatsApp+Image+2020-02-19+at+11.40.49+AM.jpeg?format=1500w" width="300" />


![George Mubanga Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1582575890458-X9GXOKLJA48S3SS64QEL/ke17ZwdGBToddI8pDm48kNBfxVd8mqxjz4YA8tKHXjxZw-zPPgdn4jUwVcJE1ZvWQUxwkmyExglNqGp0IvTJZUJFbgE-7XRK3dMEBRBhUpyM42fx3su7xom50eIO2tvy2lfhLoJPGYu8RTUqN4_T4NW_--GOQ4jiMK_MqvFk8_E/WhatsApp+Image+2020-02-19+at+11.40.48+AM+%281%29.jpeg?format=1000w)

![George Mubanga Painting](https://images.squarespace-cdn.com/content/v1/5e36b7de1ca58730c0e522af/1582575889584-ZRHEOC3J43236WMN9XO9/ke17ZwdGBToddI8pDm48kPrVQOeuIVp605YUm2FDiUgUqsxRUqqbr1mOJYKfIPR7LoDQ9mXPOjoJoqy81S2I8N_N4V1vUb5AoIIIbLZhVYxCRW4BPu10St3TBAUQYVKcMA9A18w7MX73p9Sy27KoEG8Y_prs-yYbwm5Jf3R5IAgaRlsSORtfWH3JtvVMP--8/WhatsApp+Image+2020-02-19+at+11.40.48+AM.jpeg?format=1000w)
