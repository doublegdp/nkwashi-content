---
title: Artist In Residence Program
google_analytics: ['UA-150647211-2', 'air-home']
---

# Chena Artist In Residence Program at Nkwashi

The Artist In Residence (“AIR”) Program is designed to attract new businesses and residents to Nkwashi through a sense of community and culture. Carefully selected artists from around the world are invited to live on-site, free of charge at Nkwashi for one year, with housing and studio space provided. The agreement with the artists is that they will create public art for its residents and visitors, as well as promoting and marketing Nkwashi. Artists of various media - including sculptors, visual artists, musicians, dancers - are selected to participate. The program will feature both prominent Zambian and African artists as well as some from other continents. 

Fifteen artists will live in a small neighborhood of 5 modern 3-bedroom houses, with access to a shared studio space and a bodega store for supplies. The homes are built and operated with funds for the AIR Program. 

For information on auctions, artists, and galleries, vist the Chena Art Gallery:

[Chena Art Gallery](https://www.chena-gallery.com/)


![Objectives](image_1.png))

![Building in progress](./image_2.png))

![Landscape](./image_3.png))

![House](./image_4.png))

