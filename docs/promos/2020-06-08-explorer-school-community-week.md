---
title: Explorer School Community Week
google_analytics: ['UA-150647211-2', '2020-06-08-explorer-school-community-week']
---

# Explorer School

Nkwashi is an education city! And for that reason, we're pleased to welcome the Explorer School to Nkwashi. This is a big moment for us and our community!

The Explorer School is a school without borders. All teaching happens online and is teacher led. In view of the pandemic and the general growth in remote learning and remote work, this school represents the future of education. Explorer school is not only in Zambia, nor in Africa but also the world over. Enrollment is now open. Fees start from US $20/month globally, ZMW 500/month in Zambia, and ZAR 500/month in South Africa.

<br/>

<iframe width="560" height="315" src="https://www.youtube.com/embed/8_RwPCjxiX8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br/>

## Community Week  

Throughout the week of the 2020-06-09, The Explorer School will have *Community Week*. This is a chance for teachers, parents and students to interact! Sign your child up and be a part of it!

**Contact us**  <br/>
**Phone**: +260 96 7920572/ +260 96 8975009 <br/>
**Email**: [admissions@explorer.school](mailto:admissions@explorer.school) <br/>
**Website**: [www.explorer.school](www.explorer.school)  

As always, soar high!

<br/>

<!-- ![community week](../img/community_week.jpg) -->

<img src="../../img/community_week.jpg" width="650" />
