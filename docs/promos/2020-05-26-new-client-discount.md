---
title: Discount for New Clients
google_analytics: ['UA-150647211-2', '2020-05-new-client-discount']
---

## Great Discount for New Clients

&nbsp;

Great news! Purchase any of our plots on a 5 year payment plan, at a fixed rate of K 10.00 to  1.00. Bring in two of your friends or family to purchase plots of their own on ANY payment plan and get a discount on your plot purchase value!
For more information contact 0966194383 or 0760635024. Limited plots available.

You can [refer a friend](https://app.doublegdp.com/referral) or [contact us](https://app.doublegdp.com/contact) directly for more information.

&nbsp;

<img src="../../img/new_client_discount.jpeg" width="500"/>
