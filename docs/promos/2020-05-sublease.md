---
title: Sublease Client Promotion
google_analytics: ['UA-150647211-2', '2020-05-sublease-promo']
---

<img src="../../img/nkwashi_logo.png" width="150" />

&nbsp;

### Promotion for Sublease Clients
##### May, 2020

&nbsp;

You still have a chance to benefit from our discount promotion. Pay a total of K10,000 by 31st May 2020 to receive the following month's installment absolutely free!! You can make your payment today through your [My Thebe Payment Portal](https://app.doublegdp.com/account), [MTN Mobile Money Platform](https://app.doublegdp.com/contact/mobile_money), or by visiting our offices.

And you're always welcome to [contact us](https://app.doublegdp.com/contact) for anything you need.

Sincerely,

Your Nkwashi Team

&nbsp;
&nbsp;
<img src="../../img/soar-high.png" />
