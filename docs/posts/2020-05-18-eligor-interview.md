---
title: Get Started With Building
google_analytics: ['UA-150647211-2', 'eligor-interview']
---

# House Construction Process with Eligor

Nkwashi has partnered with Eligor Construction to provide an easy, reliable, and affordable way for clients to build homes on site. We recently sat down with Eliah Phiri, the company’s managing director, and learned about the exciting progress and some of the adjustments they’ve made to keep things safe and moving forward amid Covid.

Some of the highlights include:

1. **Six houses** are underway, including a few 2-story houses
1. Workers are **staying on-site**, away from their families, to keep everyone safe and healthy
1. Eligor provides **full service construction**, including materials, labor, and transportation, so you don’t have to worry
1. Eliah **visits the site four times a week** and gives clients **regular updates**

Eligor has gone through a tendering process with Nkwashi and has been approved to build houses on site. They’re 100% Zambian-owned, and emphasize safety, transparency, and quality. They offer flexible pricing and timing, so you can build on the budget and timing that works for you.

Are you interested to start building your new home?

Reach out [here](https://app.doublegdp.com/contact) to get in touch.

<!-- Commented this out on Mutale's request  -->
<!-- Watch the full interview with Eliah Phiri   here:

<iframe width="560" height="315" src="https://www.youtube.com/embed/6xy0TfPFte0?cc_load_policy=1" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture;" allowfullscreen></iframe> -->
