---
title: New Floor Plans
google_analytics: ['UA-150647211-2', '2020-06-new-floor']
---
##  New Floor Plans

We are happy to share our brand new Floor Plans with you.

We have opened up the Nkwashi site for Home Construction and we are excited to see our community come alive. Pick a floor plan and contact our customer consultants who will assist you with more information.

**Contact us** for free via the [Contact Card](https://app.doublegdp.com/contact) in this app, or call us at +260 760 635023

Please preview Floor Plans here:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSN_fSzIzBTtdm9eWQjw_-nav1muMQA2PxUAvbecocY2sxUr6gWrxOnlKJ3VBxFVosw78hriicnaC2W/embed?start=true&loop=true&delayms=3000" frameborder="0" width="640" height="389" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

&nbsp;

<img src="../../img/soar-high.png" />
