---
title: Miss Sara Tembo Interview
google_analytics: ['UA-150647211-2', '2020-07-miss-tembo']
---


## Construction Process

&nbsp;

#### *An interview with Miss Sara Tembo,*
*Nkwashi client and soon-to-be resident*


Building your dream home is an exciting experience and we here at Nkwashi, find joy in seeing our clients fulfill their dreams. Miss Sarah Tembo not only attests to this, but shares her insightful building experience with us. Join her as she gives an insight into her journey by watching her interview below.

You could be next! Soar high!

<iframe width="560" height="315" src="https://www.youtube.com/embed/LbxnGEzHToQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
