---
title: House Construction In Progress
google_analytics: ['UA-150647211-2', '2020-06-house-construction']
---

## HOUSE CONSTRUCTION PROGRESS

We are happy to share with you a client's house construction progress!

This is a Basic House on a 505 sqm plot, and construction of the house began in September-2019. It is scheduled to be finished by the end of July-2020. The house is located in the Eagle Suburb of Tau District at the Nkwashi Housing Estate.

The entrance to this three bedroom home looks out to the street, with views of the Nkwashi hills out the front windows.

Please enjoy a few highlight pictures:

1. The front of the house with the straight staircase.
1. The view of the house from the street.
1. The living room window overlooking the outside of the house.
1. Ndele street, where the house is located.


<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-a14daba8-3b6a-45d4-a1b2-979de39b541c"></div>
