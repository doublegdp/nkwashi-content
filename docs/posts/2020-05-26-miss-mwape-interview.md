---
title: Miss Mwape Bwalya Interview
google_analytics: ['UA-150647211-2', '2020-05-miss-mwape']
---



## What’s It Like to Build Your Home At Nkwashi?

&nbsp;

#### *An interview with Miss Mwape Bwalya,*
*Nkwashi client and soon-to-be resident*


We recently sat down with Miss Mwape, who has started building her home at Nkwashi, to hear how the process is going and what advice she has for others considering starting. She shared candidly both her highlights and some of the challenges. Below are some (paraphrased) excerpts, with a full video below.

Thank you, Miss Mwape, for your pioneering spirit and openness to share!


*See the full interview here, or excerpts below*

<iframe width="560" height="315" src="https://www.youtube.com/embed/2C2tuAadAgc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;

**What excites you most about Nkwashi?**

I’ve been a client since Oct 2014. From the time the housing project started. I actually have two plots and am building on one.

What excites me about Nkwashi is I feel it’s going to be driven by a younger generation. It’s going to be a modern housing estate. I shared with you a few months ago that I’m excited about the amenities. I like that it’s very peaceful and quiet. And the view, mmm.

And the drive! Every time I go there I feel like I’m heading home. It’s very peaceful.

**So many clients will want to know, what is the process of building your home?**

You guys have been very helpful because we’re getting the plans from you. I was given two options so I don’t have to shop around. The architect sits with you, you can tell him how you want to modify it. I was really glad I had the chance to work with him from the get-go. Even when we first met, he had a vision of what a single modern woman would want in a home. Nkwashi had a list of contractors, so I picked the contractor and we got to work.

I didn’t want that hassle, so I was comforted when I didn’t have to choose. I’ve heard stories [in other places] about owners paying for people that didn’t do the work. It made it easier that there was already a list.

**What has been your highlight that you’ve enjoyed most about building your home?**

The drive there. My worry was that there would be no tarmac, worried about safety, and accessibility. But when I got there it was all set. Another highlight has been enjoying the process of picking out the design -- figuring out the walls and whether I want an open plan or not. Deciding what I want this house to be.

It’s been a beautiful experience. I’ve enjoyed it.

**It’s good to see a home owner that’s not stressed.**

No, I am stressed! <laughter> But you know, I’ve got a friend of mine who reminded me to enjoy the process and relax. I’m not a very patient person, but I’ve enjoyed the process. I wouldn’t trade it.

**What sort of updates are you receiving about your home?**

I get them from talking to you people! <laughter>

The beauty is I’m in constant communication with you, so I’m not lost. Your offices have been very open to me. I can call whenever I have a question. You call me to tell me when you’re working on the sewage tanks. Basically, You’re working with me and that’s what I like about it.

You’ve been there throughout. I can’t even explain how amazing it is.

**Do you use the application, and if you so, why?**

I use it mostly for access to the site and also to show off to other people. <laughs> I’m one of those…

**What advice would you give to our clients who are starting to build or thinking of getting started?**

First of all, start. Just get started -- there’s no point in procrastinating.
When I started, there were only the two Atlas Mara houses, but I didn’t care. I’m going to come and live here anyway. There’s no point in waiting for there to be another 20 other people. You’re one of the 20.

**Before we go, what’s one of your favorite quotes?**

“Just Do It” Sometimes it’s better to ask for forgiveness than to ask for permission

**Thank you, Miss Mwape, for being that person who just does it!**


&nbsp;
&nbsp;
<img src="../../img/soar-high.png" />
