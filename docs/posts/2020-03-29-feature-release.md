---
title: App Feature Release
google_analytics: ['UA-150647211-2', '2020-03-feature-release']
---
# Feature Release 29-Mar-20

The following updates were released in the last week:

* Added a “Support Chat” feature to the Contact Card	so clients can initiate a message with administrators. Also added help context on the app and how it makes gate access faster, easier, and more secure.
* We have officially deployed a messaging interface to allow clients to communicate directly in the app with support staff.
* Implemented an enhancement for guards: allow app to re-send the SMS authentication code to log in if they didn’t receive the first one
Implemented an enhancement for guards: sort the drop-down list of guard names to switch account.
* Added explanatory text for new client on the initial login page.
* Extended the functional period for the log-in link for newly enrolled users to 48 hours.
* Allow administrators to set an expiration date for any user type, rather than only prospective clients.
* Display home address for administrators reviewing the showroom kiosk logs
