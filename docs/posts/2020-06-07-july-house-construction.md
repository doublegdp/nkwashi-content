---
title: House Construction In Progress July
google_analytics: ['UA-150647211-2', '2020-07-sara-construction']
---

## HOUSE CONSTRUCTION PROGRESS

We are happy to share with you yet another client's house construction progress!

This is a Basic three bedroom house on a 505 sqm plot, and construction of the house began in December-2019. The house is located in the Eagle Suburb of Tau District at the Nkwashi Housing Estate.

We hope this inspires you to begin the construction process of your own home!

Please enjoy a few highlight pictures.




<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ7f_564vWy6NyvyazqMGVP6o7hsCU-VRlIWGbUWkgTF0_SeR1YGSnnd0RZeJde2LgWAyntLrqO2dVN/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


&nbsp;


<img src="../../img/soar-high.png" />


