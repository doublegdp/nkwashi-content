---
title: Tarcissius Farm Interview
google_analytics: ['UA-150647211-2', '2020-06-tarcissius-farm']
---

# What's it like to Farm at Nkwashi?

**An Interview with Tarcissius Chikopela**

We recently sat down with Tarcissius Chikopela aka Runell, a budding farmer whose farm is located in the Farm Block area. He shared his journey so far as a farmer and how he plans on being a supplier of fresh farm produce to Nkwashi Residents. 

See below the video and images of the interview.

<iframe width="560" height="315" src="https://www.youtube.com/embed/nuNdf32XiGw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRGJ_KdyO8pFbIzHHcnlj0LfPACQGvxAhqdmu6nnuhM9_6Q0DfpK5wtcn63tFzydym_bvf--_MNdH81/embed?start=true&loop=true&delayms=3000" frameborder="0" width="480" height="299" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

&nbsp;


<img src="../../img/soar-high.png" />



