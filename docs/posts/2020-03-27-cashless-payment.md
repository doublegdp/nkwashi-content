---
title: Cashless Payments
google_analytics: ['UA-150647211-2', 'cashlessPayments']
---
Stay Safe in the wake of COVID-19 (Coronavirus) pandemic by taking advantage of our various cashless payment options such as:

- the My-thebe online portal,
- Bank transfers, and
- our newly introduced MTN Mobile Money option using the number 0961722433

indicating your NRC as a reference for identification purposes.

Please [contact support](https://app.doublegdp.com/contact) for more information.
