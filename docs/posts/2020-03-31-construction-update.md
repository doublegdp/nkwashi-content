---
title: March Construction Update
google_analytics: ['UA-150647211-2', '2020-03-construction-update']
---
# Construction Update - March 2020

We've made a lot of progress in 2020! The dam is now complete. Spring rains filled the lake and brought out beautiful greens across the property. We completed construction on two houses with our Atlas Mara partners, and paved major roads connecting the gate, lake, offices, and those houses.

We're happy to share these updates, and please enjoy a few highlight pictures below:

1. The Main Nkwashi Gate
1. Interior of the Atlas Mara House's
1. One of the dams is completed and now full of water
1. Paved roads around site
1. Greenery and flowers at the Pillu Restaurant

<script src="https://apps.elfsight.com/p/platform.js" defer></script>
<div class="elfsight-app-7ddc7385-cf2f-4ab5-8fb3-940600969336" width="400"></div>
