---
title: Frequently Asked Questions
---
# Frequently Asked Questions

Here are questions and answers.

[TOC]

### Is there a shuttle service to the Nkwashi site?

Unfortunately, we've discontinued shuttle service to the site due to covid-19. We will continue to monitor the situation and see if it can be resumed. In the meantime, if you would like help arranging private transportation, please [reach us here](https://app.doublegdp.com/contact) and we will do our best to assist.

### Can I check my statement? What's my balance?

Your account balance is available through your [My Thebe portal](https://app.doublegdp.com/account). You will need to log in with your "My Thebe" username and password; if you don't remember your password, it can be reset with through the Client Request Form.

### When are clients eligible to build?  

The following are the conditions that qualify a client's eligibility to build:
1. A client pays 48 consecutive installments amounting to 80% of the total plot purchase price (Phase 1 and Phase 2).
1. A client makes 24 months worth of installments (Sublease contract).

### Does Nkwashi build for clients, and if yes how does it work?

Yes, Nkwashi does provide this facility. For more information you can visit our offices at 11 Nalikwanda Road, Woodlands, Lusaka.

### Is there investment opportunity for those interested in schools and shops?

Yes, Nkwashi welcomes investment opportunities. Proposals and queries can be emailed to [pezo.m@thebe-im.com](mailto:pezo.m@thebe-im.com) and [musawa.m@thebe-im.com](musawa.m@thebe-im.com).

### How flexible are the house plan payments?
Installment options are available, you can email [tamara.b@thebe-im.com](tamara.b@thebe-im.com)
