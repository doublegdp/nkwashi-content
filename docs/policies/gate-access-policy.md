---
title: Gate Access Policy
google_analytics: ['UA-150647211-2', 'gate-access']
---
# Gate Access Policy

The following are Nkwashi Gate Access Policies

**Note: In response to the COVID-19 crisis one of the measures we have put in place is a temperature check for clients, residents, and staff members at the gatehouse. Those with a temperature over 37º C will be asked not to enter.**

1. All who access the Nkwashi site must sign in with the guard on duty. This can be done by scanning a QR code or completing the digital entry form on the phone. This ensures Nkwashi has a record of all site visitors. Either of these methods will provide immediate notification to security administrator(s) so they can monitor activity for any concerns that guards may not notice.
1. If the internet is not functioning properly or a known client objects to using a digital log, guards may fall back to a paper log. A DoubleGDP CSM will pick up these forms at the end of each week and manually enter these. We aim to minimize this usage, since it introduces a long time delay.
1. Clients may visit the site during normal visiting hours; Residents may visit at all times. These hours are determined by the primary security administrator, shown below.
1. Security discs on a car indicate that the person is a client and should be granted access. They still should sign in with the process above, and once they complete the log should be given access to the site.
